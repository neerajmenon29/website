---
name: "Dilshad Mohammed"
avathar: "https://github.com/dilshadmohammed.png?size=200"
designation: "Design Lead"
url: "https://github.com/dilshadmohammed"
dept: "CSE"
email: "dilshadmohammed13@gmail.com"
phone: "+91 87143 81226"
skills: "Designer"
---
